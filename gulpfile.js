const fs = require('fs');
const del = require('del');

const gulp = require('gulp');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const rename = require('gulp-rename');
const autoprefixer = require('gulp-autoprefixer');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const imagemin = require('gulp-imagemin');
const file = require('gulp-file');

const themeName = 'wavewatches';
const themePath = `./`;
const images = fs
    .readdirSync(`${themePath}/src/images`)
    .map(img => `${themePath}/build/images/${img}`);



gulp.task('sass', () =>
    gulp.src([
        `${themePath}/src/scss/*.scss`,
    ])
        .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: 'compressed',
            includePaths: [
                './node_modules/bootstrap/scss',
                './node_modules/include-media/dist',
                './node_modules/normalize-scss/sass/',
                './node_modules/owl.carousel/src/scss',
            ],
        }).on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(rename({ extname: '.min.css' }))
        .pipe(sourcemaps.write('/maps'))
        .pipe(gulp.dest(`${themePath}/build/css`))
);

gulp.task('tiny-img', () =>
    gulp.src(`${themePath}/src/images/*`)
        .pipe(imagemin())
        .pipe(gulp.dest(`${themePath}/build/images`))
);

gulp.task('js-vendor', () =>
    gulp
        .src([
            'node_modules/jquery/dist/jquery.min.js',
            `${themePath}/src/js/vendor/*`,
            `./node_modules/owl.carousel/dist/owl.carousel.min.js`,
        ])
        .pipe(concat('vendor.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(`${themePath}/build/js`))
);

gulp.task('js-source', () =>
    gulp
        .src([
            `${themePath}/src/js/*`,

        ])
        .pipe(concat(`${themeName}.min.js`))
        .pipe(uglify())
        .pipe(gulp.dest(`${themePath}/build/js`))
);

gulp.task('watch', () => {
    gulp.watch(`${themePath}/src/scss/*.scss`, ['sass'])
    gulp.watch(`${themePath}/src/js/*.js`, ['js-source'])
    gulp.watch(`${themePath}/src/js/vendor/*.js`, ['js-vendor'])
});

gulp.task('clean', () =>
    del.sync([
        `${themePath}/build/`
    ])
);

gulp.task('fonts', () =>
    gulp.src(`${themePath}/src/fonts/*`)
        .pipe(gulp.dest(`${themePath}/build/fonts/`))
);

gulp.task('min-js', ['js-vendor', 'js-source']);
gulp.task('build', ['clean', 'js-vendor', 'js-source', 'tiny-img', 'sass', 'fonts']);
gulp.task('default', ['watch']);
